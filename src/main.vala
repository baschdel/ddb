
public static int main(string[] args) {
	
	string command = "server";

	if (args.length >= 2) {
		command = args[1];
	} else {
		message("Deprecation Warning: Use 'ddb server' in the future!\n");
	}
	
	if (command == "help") {
		print_usage();
		return 0;
	}
	
	string? port_str = GLib.Environment.get_variable("DDB_PORT");
	string? unix_socket_path = GLib.Environment.get_variable("DDB_SOCKET_PATH");
	string? greeting = GLib.Environment.get_variable("DDB_GREETING");
	
	if ((port_str == null && unix_socket_path == null) || greeting == null) {
		message("Missing Environment Variables, see 'ddb help'.\n");
		return 1;
	}
	
	uint16 port = 0;
	
	// Only try to evaluate the port number if given
	if (port_str != null) {
		uint64 port_parse_result = 0;
		if (DDB.Intparser.try_parse_unsigned(port_str, out port_parse_result)) {
			port = (uint16) port_parse_result;
		}

		if (port == 0) {
			message("Port must be a valid integer > 0!\n");
			return 1;
		}
	}
	
	SocketAddress[] socket_addresses = {};
	SocketAddress? primary_socket_address = null;
	
	if (port > 0) {
		if (command == "server") { message(@"Adding Tcp socket at: $port@127.0.0.1\n"); }
		var tcp_socket_address = new InetSocketAddress.from_string("127.0.0.1", port);
		socket_addresses += tcp_socket_address;
		primary_socket_address = tcp_socket_address;
	}
	
	if (unix_socket_path != null) {
		if (command == "server") { message(@"Adding Unix socket at: $unix_socket_path\n"); }
		var unix_socket_address = new UnixSocketAddress(unix_socket_path);
		socket_addresses += unix_socket_address;
		primary_socket_address = unix_socket_address;
	}
	
	if (primary_socket_address == null) {
		message("No Connection Method was specified!\n");
		return 1;
	}
	
	switch (command) {
	case "server":
		return main_server(socket_addresses, greeting);
	case "cat":
		return main_cat(primary_socket_address, greeting);
	case "read_value":
		if (args.length != 4) {
			print_usage();
			return 1;
		}
		return main_read_value(args[2], args[3], primary_socket_address, greeting);
	case "wrap":
		if (args.length <= 2) {
			print_usage();
			return 1;
		}
		return main_wrap(args[2:], primary_socket_address, greeting);
	default:
		message("Unknown command!\n");
		print_usage();
		return 1;
	}

}

public static void message(string message) {
	stderr.write(message.data);
}

public static void print_usage() {
	message("""Usage: ddb <command>
Avalable commands are:
	help - show this text
	server - starts a ddb server
	cat - starts a ddb client in cat mode
	read_value <o> <k> - reads a value from ddb to stdout
	wrap <<command>> - wraps a command in a ddb connection
DDB takes arguments via envoirenment variables:
	DDB_SOCKET_PATH: The path to the ddb unix socket
	DDB_PORT: The port ddb will listen on
	DDB_GREETING: The greeting ddb will accept

Note: If DDB_SOCKET_PATH is present it will, for clients
      be the preferred and only way to connect to ddb.

Note: Currently you have to manually clean up the unix socket.
""");
}

public static int main_server(SocketAddress[] addresses, string greeting) {
	print(@"Listening for greeting '$greeting'!\n");
	var server = new DDB.Server(addresses, greeting, new DDB.Store());
	if (!server.start()) {
		message("An error occourred while starting the server … (see above)\nHave you cleaned up the unix socket?\n");
		return 2;
	}
	new MainLoop().run();
	message("Cleaning Up …\n");
	server.socket_service.stop();
	server.socket_service.close();
	return 0;
}

public static int main_cat(SocketAddress address, string greeting) {
	int returnval = 0;
	var loop = new MainLoop();
	var stdinstream = new DataInputStream(new UnixInputStream(stdin.fileno(), false));
	var client = new DDB.ClientSocket(address);
	bool is_first_line = true;
	client.on_line_received.connect((line) => {
		if (is_first_line) {
			if (line != "Hello!") {
				client.close();
				returnval = 3;
				loop.quit();
			}
			is_first_line = false;
		}
		if (returnval == 0) {
			print(line+"\n");
		}
	});
	if (!client.initiate_connection()) {
		return 2;
	}
	client.send(greeting+"\n");
	cat_read_loop.begin(client, stdinstream, loop);
	client.read_loop.begin((obj, res) => {loop.quit();});
	loop.run();
	client.close();
	return returnval;
}

public static async void cat_read_loop(DDB.ClientSocket client, DataInputStream stdinstream, MainLoop loop) {
	string? line = null;
	try {
		while (true) {
			line = yield stdinstream.read_line_utf8_async();
			if (line == null) { break; }
			client.send(line+"\n");
			if (line == "q") { break; }
		}
	} catch (Error e) {
		//Do nothing
	}
	loop.quit();
}

public static int main_wrap(string[] args, SocketAddress address, string greeting) {
	int returnval = 0;
	var loop = new MainLoop();
	
	Pid child_pid = -1;
	int stdinfd = -1;
	int stdoutfd = -1;
	
	
	DataInputStream read_stream = null;
	DataOutputStream write_stream = null;
	var client = new DDB.ClientSocket(address);
	bool is_first_line = true;
	client.on_line_received.connect((line) => {
		if (is_first_line) {
			if (line == "Hello!") {
				try {
					Process.spawn_async_with_pipes(null, args, null, SEARCH_PATH, null, out child_pid, out stdinfd, out stdoutfd);
					read_stream = new DataInputStream(new UnixInputStream(stdoutfd, false));
					write_stream = new DataOutputStream(new UnixOutputStream(stdinfd, false));
					cat_read_loop.begin(client, read_stream, loop);
				} catch (Error e) {
					client.close();
					returnval = 5;
					loop.quit();
				}
			} else {
				client.close();
				returnval = 3;
				loop.quit();
			}
			is_first_line = false;
		}
		if (returnval == 0) {
			try {
				if (write_stream != null) {
					write_stream.put_string(line+"\n");
				}
			} catch (Error e) {
				client.close();
				loop.quit();
			}
		}
	});
	if (!client.initiate_connection()) {
		return 2;
	}
	client.send(greeting+"\n");
	client.read_loop.begin((obj, res) => {loop.quit();});
	loop.run();
	try {
		if (write_stream != null) {
			write_stream.close();
		}
	} catch (Error e) {}
	try {
		if (read_stream != null) {
			read_stream.close();
		}
	} catch (Error e) {}
	client.close();
	return returnval;
}

public static int main_read_value(string object, string key, SocketAddress address, string greeting) {
	if (object.contains(" ") || object.contains("\n") ||
	    key.contains(" ") || key.contains("\n")) {
		return 1;
	}
	int returnval = 0;
	var loop = new MainLoop();
	var client = new DDB.ClientSocket(address);
	bool is_first_line = true;
	client.on_line_received.connect((line) => {
		bool quit = false;
		if (is_first_line) {
			if (line != "Hello!") {
				client.close();
				returnval = 3;
				loop.quit();
			} else {
				client.send(@"r $object $key\n");
			}
			is_first_line = false;
		} else if (line == "ERROR") {
			returnval = 4;
			quit = true;
		} else if (line == "OK") {
			//Ignore ok lines
		} else if (line.has_prefix("> ")) {
			var args = line.split(" ", 4);
			if (args.length == 4) {
				if (args[1] == object && args[2] == key) {
					print(args[3]+"\n");
					quit = true;
				}
			} else {
				returnval = 3;
				quit = true;
			}
		} else if (line.has_prefix("u ")) {
			if (line == @"u $object $key") {
				print("\n");
				quit = true;	
			}
		}
		if (quit) {
			client.send("q\n");
			loop.quit();
		}
	});
	if (!client.initiate_connection()) {
		return 2;
	}
	client.send(greeting+"\n");
	client.read_loop.begin((obj, res) => {loop.quit();});
	loop.run();
	client.close();
	return returnval;
}
