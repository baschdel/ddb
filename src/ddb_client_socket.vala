public class DDB.ClientSocket : Object {
	
	private SocketAddress address;
	private SocketConnection? connection;
	private DataInputStream? input_stream = null;
	private DataOutputStream? output_stream = null;
	
	public ClientSocket(SocketAddress address) {
		this.address = address;
	}
	
	public bool is_connected(){
		lock (connection) {
			if (connection != null) {
				return connection.is_connected();
			} else {
				return false;
			}
		}
	}
	
	public bool close(){
		lock (connection) {
			if (connection != null) {
				try {
					input_stream.close();
					output_stream.close();
					if (connection.close()) {
						connection = null;
						return true;
					} else {
						return false;	
					}
				} catch (Error e) {
					return false;
				}
			} else {
				return false;
			}
		}
	}
	
	public bool initiate_connection(){
		InputStream? input = null;
		OutputStream? output = null;
		lock (connection) {
			if (connection != null) {
				return false;
			}
			var client = new SocketClient();
			try {
				connection = client.connect(address);
				if (connection == null) {
					return false;
				}
				input = connection.get_input_stream();
				output = connection.get_output_stream();
			} catch (Error e) {
				connection = null;
				return false;
			}
		}
		if (input == null || output == null) {
			return false;
		}
		input_stream = new DataInputStream(input);
		output_stream = new DataOutputStream(output);
		return true;
	}
	
	public async bool read_loop() {
		string? line = null;
		try {
			while ((line = yield input_stream.read_line_utf8_async()) != null) {
				if (line == null) {
					break;
				}
				on_line_received(line);
			}
		} catch (Error e) {
			connection = null;
			input_stream = null;
			output_stream = null;
		}
		this.close();
		return true;
	}
	
	public bool send(string text){
		lock (connection) {
			if (connection == null) {
				return false;
			}
			try {	
				output_stream.put_string(text);
				return true;
			}	catch (Error e) {
				return false;
			}
		}
	}
	
	public signal void on_line_received(string line);
}
