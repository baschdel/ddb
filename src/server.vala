public class DDB.Server : Object {
	
	public string greeting;
	public SocketService socket_service = new SocketService();
	public DDB.Store object_store;
	private SocketAddress[] socket_addresses;
	
	public Server(SocketAddress[] socket_addresses, string greeting, DDB.Store object_store){
		this.greeting = greeting;
		this.object_store = object_store;
		this.socket_addresses = socket_addresses;
		socket_service.incoming.connect(this.on_incoming_connection);
	}
	
	public bool start() {
		try {
			//InetSocketAddress inetaddress = new InetSocketAddress.from_string("127.0.0.1", port);
			//socket_service.add_address(inetaddress, SocketType.STREAM, SocketProtocol.TCP, null, null);
			foreach (SocketAddress address in socket_addresses) {
				socket_service.add_address(address, SocketType.STREAM, SocketProtocol.DEFAULT, null, null);
			}
			socket_service.start();
			return true;
		} catch (Error e) {
			critical("Error while setting up server: "+e.message);
			return false;
		}
	}
	
	public bool on_incoming_connection (SocketConnection connection) {
		info("Incoming connaction");
		handle_conection.begin(connection);
		return true;
	}

	public async void handle_conection(SocketConnection connection) {
		try {
			var input_stream = new DataInputStream(connection.input_stream);
			var output_stream = new DataOutputStream(connection.output_stream);
			string read_line = yield input_stream.read_line_async(Priority.HIGH_IDLE);
			if (this.greeting == read_line) {
				output_stream.put_string("Hello!\n");
				var signal_serializer = new DDB.SignalSerializer(object_store, output_stream);
				while ((read_line = yield input_stream.read_line_async(Priority.HIGH_IDLE)) != null) {
					if (read_line == "q") { break; }
					if (read_line.has_prefix("> ") || read_line.has_prefix("r ") || read_line.has_prefix("s ") || read_line.has_prefix("u ") || read_line.has_prefix("+ ") || read_line.has_prefix("- ")) {
						string[] args = read_line.split(" ", 4);
						switch (args[0]) {
							case ">": //set property
								if (args.length == 4) {
									string object = args[1];
									string key = args[2];
									string val  = args[3];
									bool success = false;
									if (key == "type") {
										success = object_store.create_object(object, val);
									} else {
										success = object_store.set_object_property(object, key, val);
									}
									if (success) {
										output_stream.put_string("OK\n");
									} else {
										output_stream.put_string("ERROR\n");
									}
								} else {
									output_stream.put_string("ERROR\n");
								}
								break;
							case "u": //unset property
								if (args.length == 3) {
									bool success = false;
									if (args[2] == "type") {
										success = object_store.remove_object(args[1]);
									} else {
										success = object_store.set_object_property(args[1], args[2], args[3]);
									}
									if (success) {
										output_stream.put_string("OK\n");
									} else {
										output_stream.put_string("ERROR\n");
									}
								} else {
									output_stream.put_string("ERROR\n");
								}
								break;
							case "r": //request property
								if (args.length == 3) {
									string? result = object_store.get_object_property(args[1], args[2]);
									if (result != null) {
										output_stream.put_string(@"> $(args[1]) $(args[2]) $result\n");
									} else {
										output_stream.put_string(@"u $(args[1]) $(args[2])\n");
									}
									output_stream.put_string("OK\n");
								} else {
									output_stream.put_string("ERROR\n");
								}
								break;
							case "s": //signal
								if (args.length == 3) {
									object_store.send_signal(args[1], args[2]);
									output_stream.put_string("OK\n");
								} else {
									output_stream.put_string("ERROR\n");
								}
								break;
							case "+": //subscribe to object
								if (args.length == 2) {
									signal_serializer.subscribe(args[1]);
									output_stream.put_string("OK\n");
								} else {
									output_stream.put_string("ERROR\n");
								}
								break;
							case "-": //unsubscribe from object
								if (args.length == 2) {
									signal_serializer.unsubscribe(args[1]);
									output_stream.put_string("OK\n");
								} else {
									output_stream.put_string("ERROR\n");
								}
								break;
							default:
								critical("This code should not be executed!");
								break;
						}
					} else {
						output_stream.put_string("ERROR\n");
					}
				}
			} else {
				warning("Greeting mismatch, denying connection.");
			}
			input_stream.close();
			output_stream.close();
			connection.close();
		} catch (Error e) {
			critical("Error while handling connection: "+e.message);
		}
	}
	
}
