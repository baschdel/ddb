public class DDB.SignalSerializer : Object {
	
	private DDB.Store object_store;
	private DataOutputStream output_stream;
	private GenericSet<string> subscribed_objects = new GenericSet<string>(str_hash, str_equal);
	
	public SignalSerializer(DDB.Store object_store, DataOutputStream output_stream) {
		this.object_store = object_store;
		this.output_stream = output_stream;
		this.object_store.object_created.connect(on_object_created);
		this.object_store.object_removed.connect(on_object_removed);
		this.object_store.value_updated.connect(on_value_updated);
		this.object_store.signal_fired.connect(on_signal_fired);
	}
	
	~SignalSerializer() {
		this.object_store.object_created.disconnect(on_object_created);
		this.object_store.object_removed.disconnect(on_object_removed);
		this.object_store.value_updated.disconnect(on_value_updated);
		this.object_store.signal_fired.disconnect(on_signal_fired);
	}
	
	public void subscribe(string object_name) {
		subscribed_objects.add(object_name);
	}
	
	public void unsubscribe(string object_name) {
		subscribed_objects.remove(object_name);
	}
	
	private void submit_packet(string packet) {
		try {
			output_stream.put_string(@"$packet\n");
		} catch (Error e) {
			critical("Error while relaying signal: "+e.message);
		}
	}
	
	private void on_value_updated(string obj_name, string key, string? val){
		if (subscribed_objects.contains(obj_name)) {
			if (val == null) {
				submit_packet(@"u $obj_name $key");
			} else {
				submit_packet(@"> $obj_name $key $val");
			}
		}
	}
	
	private void on_object_created(string obj_name, string obj_type){
		if (subscribed_objects.contains(obj_name)) {
			submit_packet(@"> $obj_name type $obj_type");
		}
	}
	
	private void on_object_removed(string obj_name){
		if (subscribed_objects.contains(obj_name)) {
			submit_packet(@"u $obj_name type");
		}
	}
	
	private void on_signal_fired(string obj_name, string signal_name){
		if (subscribed_objects.contains(obj_name)) {
			submit_packet(@"s $obj_name $signal_name");
		}
	}
	
}
