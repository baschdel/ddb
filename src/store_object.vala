public class DDB.StoreObject : Object {
	
	private HashTable<string, string> values = new HashTable<string, string>(str_hash, str_equal);
	
	public StoreObject(string obj_type) {
		this.values.set("type", obj_type);
	}
	
	public bool set_object_property(string key, string? val) {
		if (key == "type") {
			return false;
		} else {	
			if (val != null) {
				this.values.set(key, val);
			} else {
				this.values.remove(key);
			}
			return true;
		}
	}
	
	public string? get_object_property(string key) {
		return values.get(key);
	}
}
