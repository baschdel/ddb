#!/usr/bin/lua
local object,key = ...
if not (object and key) then
	print("Usage:")
	print("\tddb_read_value.lua <object> <key>")
	print("")
	print("Wrap in ddb_connect.")
	return
end

print("r "..object.." "..key)

while true do
	local i = io.read()
	if not i then break end
	if i == "ERROR" then
		print("q")
		break
	end
	local o,k,v = i:match("^> (.-) (.-) (.+)$")
	if o == object and k == key then
		io.stderr:write(tostring(v).."\n")
		print("q")
		break
	end
	local o,k = i:match("^u (.-) ([^ ]+)$")
	if o == object and k == key then
		print("q")
		break
	end
end

