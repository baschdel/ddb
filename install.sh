#!/bin/bash

bash ./build

cp -f ddb ~/.local/bin/ddb
cp -f ddb_connect ~/.local/bin/ddb_connect
cp -f ddb_cat ~/.local/bin/ddb_cat
cp -f ddb_read_value.lua ~/.local/bin/ddb_read_value

chmod +x ~/.local/bin/ddb
chmod +x ~/.local/bin/ddb_connect
chmod +x ~/.local/bin/ddb_cat
chmod +x ~/.local/bin/ddb_read_value
